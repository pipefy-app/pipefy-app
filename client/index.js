/* globals PipefyApp */
import React from 'react';
import { render } from 'react-dom';

const pipefy = PipefyApp.init();

class ReactSample extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = { pipe: null };
  }
  componentDidMount() {
    PipefyApp.render(() => {
      pipefy.pipe().then((pipe) => {
        this.setState({ pipe })
      });
    });
  }
  
  render() {
    if (!this.state.pipe) { return <div /> };
    return <iframe 
             src="https://openbanking.keycash.io/version-test/openbanking-dev" title="Open Banking"
             height="1000"
             width="1000"
             frameborder="0"
             ></iframe>
  }
}


render(<ReactSample />, document.getElementById('application'));